﻿local define INTERFACE_USER "xxx"

local define INTERFACE_PWD "xxx"

insert into bubs_pre_laden_r@ora(action_requested, action_parameter1, action_parameter2, action_parameter3, action_parameter4, action_parameter9) values ('LOGON FULL', 'xx', 'act', '${INTERFACE_USER}', 'xx', '${INTERFACE_PWD}')

--
-- Register Exact Online tables as tables in Invantive Producer.
--
insert into itgen_tables_v@ora
( apn_code
, tbe_code
, tbe_name
, tbe_icon_url
, tbe_legacy_name
, tbe_default_ref_columns
, tbe_default_select_columns
, tbe_derived_flag
, tbe_label_singular
, tbe_partition_flag
, tbe_label_plural
, tbe_label_singular_ref
, tbe_label_plural_ref
, tbe_create_flag
, tbe_is_interface_flag
, tbe_create_history_flag
, tbe_show_history_flag
, tbe_attach_documents_flag
, tbe_number_rows
, tbe_definition
, tbe_example
, tbe_documentation_l
, tbe_grant_select_flag
, tbe_grant_insert_flag
, tbe_grant_update_flag
, tbe_grant_delete_flag
, tbe_data_ind
, tbe_autocompleted_flag
, tbe_orig_system_reference
)
select 'repos/eol/trunk' apn_code
,      coalesce(tbe.short_name, replace(tbe.catalog, 'ExactOnline', '') || '.' || tbe.schema || '.' || tbe.name) tbe_code
,      tbe.catalog || '.' || tbe.schema || '.' || tbe.name tbe_name
,      null tbe_icon_url
,      null tbe_legacy_name
,      'id' tbe_default_ref_columns
,      null tbe_default_select_columns
,      false tbe_derived_flag
,      coalesce(tbe.label_singular, '?') tbe_label_singular
,      tbe.is_partition_specific tbe_partition_flag
,      coalesce(tbe.label_plural, '?') tbe_label_plural
,      coalesce(tbe.label_singular, '?') tbe_label_singular_ref
,      coalesce(tbe.label_plural, '?') tbe_label_plural_ref
,      false tbe_create_flag
,      false tbe_is_interface_flag
,      false tbe_create_history_flag
,      false tbe_show_history_flag
,      false tbe_attach_documents_flag
,      tbe.estimated_number_of_rows tbe_number_rows
,      coalesce(tbe.description, 'None.') tbe_definition
,      coalesce(tbe.example, 'None.') tbe_example
,      tbe.documentation tbe_documentation_l
,      tbe.can_select tbe_grant_select_flag
,      tbe.can_insert tbe_grant_insert_flag
,      tbe.can_update tbe_grant_update_flag
,      tbe.can_delete tbe_grant_delete_flag
,      'T' tbe_data_ind
,      false tbe_autocompleted_flag
,      tbe.catalog || '.' || tbe.schema || '.' || tbe.name tbe_orig_system_reference
from   systemtables@datadictionary tbe
where  tbe.data_container_alias = 'eol'
and    tbe.catalog || '.' || tbe.schema || '.' || tbe.name not in ( select tbe_orig_system_reference from itgen_tables_v@ora where tbe_name is not null and apn_code = 'repos/eol/trunk' )

--
-- Load table columns into Invantive Producer repository for the REST API-based tables.
--
insert into itgen_table_columns_v@ora
( apn_code
, tbe_code
, tcn_name
, tcn_legacy_name
, tcn_data_type
, tcn_data_length
, tcn_data_precision
, tcn_data_scale
, tcn_nullable_flag
, tcn_display_filter_flag
, tcn_display_results_flag
, tcn_display_lov_flag
, tcn_display_record_flag
, tcn_derived_flag
, tcn_label_heading
, tcn_label_singular
, tcn_label_plural
, tcn_confidential_flag
, tcn_display_control_name
, tcn_display_control_options
, tcn_display_order
, tcn_user_changeable_flag
, tcn_label_singular_ref
, tcn_label_plural_ref
, tcn_create_flag
, tcn_definition
, tcn_example
, tcn_add_order
, tcn_conversion
, tcn_documentation_l
, tcn_definition_manual_l
, tcn_default_value_expression
, tcn_spell_check_flag
, tcn_translatable_flag
, tcn_data_length_type
, tcn_orig_system_reference
)
select 'repos/eol/trunk' apn_code
,      tbe.tbe_code tbe_code
,      tcn.name tcn_name
,      null tcn_legacy_name
,      tcn.database_data_type tcn_data_type
,      case
       when tcn.database_data_type in ( 'varchar2', 'varchar', 'char' )
       then coalesce(tcn.max_length, 240)
       else null
       end
       tcn_data_length
,      tcn.precision tcn_data_precision
,      tcn.scale tcn_data_scale
,      tcn.nullable tcn_nullable_flag
,      'N' tcn_display_filter_flag
,      'N' tcn_display_results_flag
,      'N' tcn_display_lov_flag
,      'N' tcn_display_record_flag
,      'N' tcn_derived_flag
,      null tcn_label_heading
,      tcn.label_singular tcn_label_singular
,      null tcn_label_plural
,      'N' tcn_confidential_flag
,      null tcn_display_control_name
,      null tcn_display_control_options
,      null tcn_display_order
,      'N' tcn_user_changeable_flag
,      null tcn_label_singular_ref
,      null tcn_label_plural_ref
,      'N' tcn_create_flag
,      coalesce(tcn.documentation, 'None.') tcn_definition
,      'None.' tcn_example
,      '20171025' tcn_add_order
,      ':orig' tcn_conversion
,      null tcn_documentation_l
,      null tcn_definition_manual_l
,      null tcn_default_value_expression
,      'N' tcn_spell_check_flag
,      'N' tcn_translatable_flag
,      case
       when tcn.database_data_type = 'VARCHAR2'
       then 'C'
       else null
       end
       tcn_data_length_type
,      tcn.table_catalog || '.' || tcn.table_schema || '.' || tcn.table_name || '.' || tcn.name
from   systemtablecolumnslive@datadictionary tcn
join   itgen_tables_v@ora tbe
on     tbe.apn_code = 'repos/eol/trunk'
and    tbe.tbe_orig_system_reference = tcn.table_catalog || '.' || tcn.table_schema || '.' || tcn.table_name
where  tcn.table_catalog || '.' || tcn.table_schema || '.' || tcn.table_name || '.' || tcn.name not in ( select tcn.tcn_orig_system_reference from itgen_table_columns_v@ora tcn where tcn.apn_code = 'repos/eol/trunk' )
and    tcn.database_data_type is not null /* Exact Online has some invalid data types. */