﻿--
-- Replicate data from Exact Online and NMBRS to a SQL Server environment.
--
-- Remember to put the settings-*.xml in your invantive folder first.
--

use all@eol, all@nmbrs

create or replace table load_nmbrs_employees@sqlserver
as
select * 
from   employees@nmbrs

create or replace table load_exactonline_employees@sqlserver
as
select divisionlabel
,      code
,      fullname
,      city 
from   exactonlinerest..employees@eol

