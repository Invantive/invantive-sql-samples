﻿set use-http-disk-cache false

set use-http-memory-cache false

--
-- Source customer data.
--
use :EOL_SOURCE_DIVISION

--
-- Create 10.000 rows.
--

create or replace table testset10000@inmemorystorage
as
select act.*
from   ( select act.* 
         from   exactonlinerest..accounts act
         limit  2500
       ) act
join   range(4)@datadictionary

create or replace table testset1000@inmemorystorage
as
select act.*
from   ( select act.* 
         from   exactonlinerest..accounts act
         limit  1000
       ) act

--
-- Create Exact Online XML per account.
--
create or replace table testsetxmlaccount1000@inmemorystorage
as
select '<Account searchcode="'
       || searchcode
       || '" status="'
       || status
       || '" code="'
       || '{code}'
       || '">'
       || xmlelement('Name', '{code}-' || name, false)
       || xmlelement('Phone', phone, false)
       || xmlelement('PhoneExt', phoneextension, false)
       || xmlelement('Fax', fax, false)
       || xmlelement('Email', email, false)
       || xmlelement('HomePage', email, false)
       || xmlelement('IsSupplier', issupplier, false)
       || xmlelement('CanDropShip', candropship, false)
       || xmlelement('IsBlocked', blocked, false)
       || xmlelement('IsReseller', isreseller, false)
       || xmlelement('IsSales', IsSales, false)
       || xmlelement('IsPurchase', IsPurchase, false)
       || xmlelement('VATNumber', VATNumber, false)
       || xmlelement('VATLiability', VATLiability, false)
       || xmlelement('ChamberOfCommerce', ChamberOfCommerce, false)
       || xmlelement('ChamberOfCommerceEstablishment', EstablishedDate, false)
       || xmlelement('IsMailing', IsMailing, false)
       || xmlelement('SalesCurrency', SalesCurrency, false)
       || xmlelement('PurchaseCurrency', PurchaseCurrency, false)
       || xmlelement('IsCompetitor', IsCompetitor, false)
       || xmlelement('StartDate', StartDate, false)
       || xmlelement('InvoicingMethod', InvoicingMethod, false)
       || '</Account>'
       xml
from   testset1000@inmemorystorage


--
-- Switch to loading environment Exact Online.
--
use :EOL_TARGET_DIVISION

set requests-parallel-max 1

local remark EI1

insert into exactonlinerest..accounts
select Accountant
,      AccountManager
,      AccountManagerFullName
,      AccountManagerHID
,      ActivitySector
,      ActivitySubSector
,      AddressLine1
,      AddressLine2
,      AddressLine3
,      Blocked
,      BRIN
,      BusinessType
,      CanDropShip
,      ChamberOfCommerce
,      City
,      null Classification
,      null Classification1
,      null Classification2
,      null Classification3
,      null Classification4
,      null Classification5
,      null Classification6
,      null Classification7
,      null Classification8
,      null ClassificationDescription
,      null Code
,      CodeAtSupplier
,      CompanySize
,      ConsolidationScenario
,      ControlledDate
,      Costcenter
,      CostcenterDescription
,      CostPaid
,      Country
,      CountryName
,      Created
,      Creator
,      CreatorFullName
,      CreditLinePurchase
,      CreditLineSales
,      Currency
,      CustomerSince
,      DatevCreditorCode
,      DatevDebtorCode
,      DiscountPurchase
,      DiscountSales
,      null Division
,      Document
,      DunsNumber
,      Email
,      EndDate
,      EstablishedDate
,      Fax
,      GLAccountPurchase
,      GLAccountSales
,      GLAP
,      GLAR
,      HasWithholdingTaxSales
,      null ID
,      IgnoreDatevWarningMessage
,      IntraStatArea
,      IntraStatDeliveryTerm
,      IntraStatSystem
,      IntraStatTransactionA
,      IntraStatTransactionB
,      IntraStatTransportMethod
,      InvoiceAccount
,      InvoiceAccountCode
,      InvoiceAccountName
,      InvoiceAttachmentType
,      InvoicingMethod
,      IsAccountant
,      IsAgency
,      IsBank
,      IsCompetitor
,      IsExtraDuty
,      IsMailing
,      IsMember
,      IsPilot
,      IsPurchase
,      IsReseller
,      IsSales
,      IsSupplier
,      Language
,      OINNumber
,      LanguageDescription
,      Latitude
,      LeadPurpose
,      LeadSource
,      Logo
,      LogoFileName
,      LogoThumbnailUrl
,      LogoUrl
,      Longitude
,      null MainContact
,      Modified
,      Modifier
,      ModifierFullName
,      Name
,      Parent
,      PayAsYouEarn
,      PaymentConditionPurchase
,      PaymentConditionPurchaseDescription
,      PaymentConditionSales
,      PaymentConditionSalesDescription
,      Phone
,      PhoneExtension
,      Postcode
,      PriceList
,      PurchaseCurrency
,      PurchaseCurrencyDescription
,      PurchaseLeadDays
,      null PurchaseVATCode
,      PurchaseVATCodeDescription
,      RecepientOfCommissions
,      Remarks
,      Reseller
,      ResellerCode
,      ResellerName
,      RSIN
,      SalesCurrency
,      SalesCurrencyDescription
,      SalesTaxSchedule
,      SalesTaxScheduleCode
,      SalesTaxScheduleDescription
,      null SalesVATCode
,      SalesVATCodeDescription
,      SearchCode
,      SecurityLevel
,      SeparateInvPerProject
,      SeparateInvPerSubscription
,      ShippingLeadDays
,      ShippingMethod
,      StartDate
,      State
,      StateName
,      Status
,      StatusSince
,      TradeName
,      Type
,      UniqueTaxpayerReference
,      VATLiability
,      VATNumber
,      Website
from   testset1000@inmemorystorage

local remark EB1

bulk insert into exactonlinerest..accounts
select Accountant
,      AccountManager
,      AccountManagerFullName
,      AccountManagerHID
,      ActivitySector
,      ActivitySubSector
,      AddressLine1
,      AddressLine2
,      AddressLine3
,      Blocked
,      BRIN
,      BusinessType
,      CanDropShip
,      ChamberOfCommerce
,      City
,      null Classification
,      null Classification1
,      null Classification2
,      null Classification3
,      null Classification4
,      null Classification5
,      null Classification6
,      null Classification7
,      null Classification8
,      null ClassificationDescription
,      null Code
,      CodeAtSupplier
,      CompanySize
,      ConsolidationScenario
,      ControlledDate
,      Costcenter
,      CostcenterDescription
,      CostPaid
,      Country
,      CountryName
,      Created
,      Creator
,      CreatorFullName
,      CreditLinePurchase
,      CreditLineSales
,      Currency
,      CustomerSince
,      DatevCreditorCode
,      DatevDebtorCode
,      DiscountPurchase
,      DiscountSales
,      :EOL_TARGET_DIVISION Division
,      Document
,      DunsNumber
,      Email
,      EndDate
,      EstablishedDate
,      Fax
,      GLAccountPurchase
,      GLAccountSales
,      GLAP
,      GLAR
,      HasWithholdingTaxSales
,      null ID
,      IgnoreDatevWarningMessage
,      IntraStatArea
,      IntraStatDeliveryTerm
,      IntraStatSystem
,      IntraStatTransactionA
,      IntraStatTransactionB
,      IntraStatTransportMethod
,      InvoiceAccount
,      InvoiceAccountCode
,      InvoiceAccountName
,      InvoiceAttachmentType
,      InvoicingMethod
,      IsAccountant
,      IsAgency
,      IsBank
,      IsCompetitor
,      IsExtraDuty
,      IsMailing
,      IsMember
,      IsPilot
,      IsPurchase
,      IsReseller
,      IsSales
,      IsSupplier
,      Language
,      OINNumber
,      LanguageDescription
,      Latitude
,      LeadPurpose
,      LeadSource
,      Logo
,      LogoFileName
,      LogoThumbnailUrl
,      LogoUrl
,      Longitude
,      null MainContact
,      Modified
,      Modifier
,      ModifierFullName
,      Name
,      Parent
,      PayAsYouEarn
,      PaymentConditionPurchase
,      PaymentConditionPurchaseDescription
,      PaymentConditionSales
,      PaymentConditionSalesDescription
,      Phone
,      PhoneExtension
,      Postcode
,      PriceList
,      PurchaseCurrency
,      PurchaseCurrencyDescription
,      PurchaseLeadDays
,      null PurchaseVATCode
,      PurchaseVATCodeDescription
,      RecepientOfCommissions
,      Remarks
,      Reseller
,      ResellerCode
,      ResellerName
,      RSIN
,      SalesCurrency
,      SalesCurrencyDescription
,      SalesTaxSchedule
,      SalesTaxScheduleCode
,      SalesTaxScheduleDescription
,      null SalesVATCode
,      SalesVATCodeDescription
,      SearchCode
,      SecurityLevel
,      SeparateInvPerProject
,      SeparateInvPerSubscription
,      ShippingLeadDays
,      ShippingMethod
,      StartDate
,      State
,      StateName
,      Status
,      StatusSince
,      TradeName
,      Type
,      UniqueTaxpayerReference
,      VATLiability
,      VATNumber
,      Website
from   ( select * from testset1000@inmemorystorage limit 50 ) /* Circumvent bug ITGEN-2767. */
join   range(20)@datadictionary

local remark EX1

select max(to_number(code)) from exactonlinerest..accounts

--
-- Create complete XML message for Exact Online XML. Replace 10000 by max from previous statement to ensure new accounts getting created.
--
create or replace table testsetxmlaccounts1000@inmemorystorage
as
select xmlformat
       ( '<?xml version="1.0" encoding="utf-8"?><eExact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="eExact-XML.xsd"><Accounts>'
         || xml
         || '</Accounts></eExact>'
       )
       xml
from   ( select listagg(replace(xml, '{code}', 15000 + rowid$), '') xml from testsetxmlaccount1000@inmemorystorage )

insert into UploadXMLTopics
( topic
, payload
, division_code
, fragment_payload_flag
, fragment_max_size_characters
)
select 'Accounts'
,      xml
,      :EOL_TARGET_DIVISION
,      true
,      50000
from   testsetxmlaccounts1000@inmemorystorage

select * from UploadXMLTopicFragments

--
-- Switch to loading environment Exact Online.
--
use select code from systemdivisions where description like 'Leeg %' order by description limit 100

create or replace table testset10000@inmemorystorage
as
select act.Accountant
,      act.AccountManager
,      act.AccountManagerFullName
,      act.AccountManagerHID
,      act.ActivitySector
,      act.ActivitySubSector
,      act.AddressLine1
,      act.AddressLine2
,      act.AddressLine3
,      act.Blocked
,      act.BRIN
,      act.BusinessType
,      act.CanDropShip
,      act.ChamberOfCommerce
,      act.City
,      null Classification
,      null Classification1
,      null Classification2
,      null Classification3
,      null Classification4
,      null Classification5
,      null Classification6
,      null Classification7
,      null Classification8
,      act.ClassificationDescription
,      null Code
,      act.CodeAtSupplier
,      act.CompanySize
,      act.ConsolidationScenario
,      act.ControlledDate
,      act.Costcenter
,      act.CostcenterDescription
,      act.CostPaid
,      act.Country
,      act.CountryName
,      act.Created
,      act.Creator
,      act.CreatorFullName
,      act.CreditLinePurchase
,      act.CreditLineSales
,      act.Currency
,      act.CustomerSince
,      act.DatevCreditorCode
,      act.DatevDebtorCode
,      act.DiscountPurchase
,      act.DiscountSales
,      spn.Code Division
,      act.Document
,      act.DunsNumber
,      act.Email
,      act.EndDate
,      act.EstablishedDate
,      act.Fax
,      act.GLAccountPurchase
,      act.GLAccountSales
,      act.GLAP
,      act.GLAR
,      act.HasWithholdingTaxSales
,      null ID
,      act.IgnoreDatevWarningMessage
,      act.IntraStatArea
,      act.IntraStatDeliveryTerm
,      act.IntraStatSystem
,      act.IntraStatTransactionA
,      act.IntraStatTransactionB
,      act.IntraStatTransportMethod
,      act.InvoiceAccount
,      act.InvoiceAccountCode
,      act.InvoiceAccountName
,      act.InvoiceAttachmentType
,      act.InvoicingMethod
,      act.IsAccountant
,      act.IsAgency
,      act.IsBank
,      act.IsCompetitor
,      act.IsExtraDuty
,      act.IsMailing
,      act.IsMember
,      act.IsPilot
,      act.IsPurchase
,      act.IsReseller
,      act.IsSales
,      act.IsSupplier
,      act.Language
,      act.OINNumber
,      act.LanguageDescription
,      act.Latitude
,      act.LeadPurpose
,      act.LeadSource
,      act.Logo
,      act.LogoFileName
,      act.LogoThumbnailUrl
,      act.LogoUrl
,      act.Longitude
,      null MainContact
,      act.Modified
,      act.Modifier
,      act.ModifierFullName
,      act.Name
,      act.Parent
,      act.PayAsYouEarn
,      act.PaymentConditionPurchase
,      act.PaymentConditionPurchaseDescription
,      act.PaymentConditionSales
,      act.PaymentConditionSalesDescription
,      act.Phone
,      act.PhoneExtension
,      act.Postcode
,      act.PriceList
,      null PurchaseCurrency
,      act.PurchaseCurrencyDescription
,      act.PurchaseLeadDays
,      null PurchaseVATCode
,      act.PurchaseVATCodeDescription
,      act.RecepientOfCommissions
,      act.Remarks
,      act.Reseller
,      act.ResellerCode
,      act.ResellerName
,      act.RSIN
,      act.SalesCurrency
,      act.SalesCurrencyDescription
,      act.SalesTaxSchedule
,      act.SalesTaxScheduleCode
,      act.SalesTaxScheduleDescription
,      null SalesVATCode
,      act.SalesVATCodeDescription
,      act.SearchCode
,      act.SecurityLevel
,      act.SeparateInvPerProject
,      act.SeparateInvPerSubscription
,      act.ShippingLeadDays
,      act.ShippingMethod
,      act.StartDate
,      act.State
,      act.StateName
,      act.Status
,      act.StatusSince
,      act.TradeName
,      act.Type
,      act.UniqueTaxpayerReference
,      act.VATLiability
,      act.VATNumber
,      act.Website
from   ( select code from systempartitions@datadictionary spn where spn.is_selected = true  ) spn
join   ( select * from testset1000@inmemorystorage act limit 2 ) act
join   range(50)@datadictionary

local remark EI10

set requests-parallel-max 100

insert into exactonlinerest..accounts
select ACCOUNTANT
,      ACCOUNTMANAGER
,      ACCOUNTMANAGERFULLNAME
,      ACCOUNTMANAGERHID
,      ACTIVITYSECTOR
,      ACTIVITYSUBSECTOR
,      ADDRESSLINE1
,      ADDRESSLINE2
,      ADDRESSLINE3
,      BLOCKED
,      BRIN
,      BUSINESSTYPE
,      CANDROPSHIP
,      CHAMBEROFCOMMERCE
,      CITY
,      Classification
,      Classification1
,      Classification2
,      Classification3
,      Classification4
,      Classification5
,      Classification6
,      Classification7
,      Classification8
,      CLASSIFICATIONDESCRIPTION
,      Code
,      CODEATSUPPLIER
,      COMPANYSIZE
,      CONSOLIDATIONSCENARIO
,      CONTROLLEDDATE
,      COSTCENTER
,      COSTCENTERDESCRIPTION
,      COSTPAID
,      COUNTRY
,      COUNTRYNAME
,      CREATED
,      CREATOR
,      CREATORFULLNAME
,      CREDITLINEPURCHASE
,      CREDITLINESALES
,      CURRENCY
,      CUSTOMERSINCE
,      DATEVCREDITORCODE
,      DATEVDEBTORCODE
,      DISCOUNTPURCHASE
,      DISCOUNTSALES
,      Division
,      DOCUMENT
,      DUNSNUMBER
,      EMAIL
,      ENDDATE
,      ESTABLISHEDDATE
,      FAX
,      GLACCOUNTPURCHASE
,      GLACCOUNTSALES
,      GLAP
,      GLAR
,      HASWITHHOLDINGTAXSALES
,      ID
,      IGNOREDATEVWARNINGMESSAGE
,      INTRASTATAREA
,      INTRASTATDELIVERYTERM
,      INTRASTATSYSTEM
,      INTRASTATTRANSACTIONA
,      INTRASTATTRANSACTIONB
,      INTRASTATTRANSPORTMETHOD
,      INVOICEACCOUNT
,      INVOICEACCOUNTCODE
,      INVOICEACCOUNTNAME
,      INVOICEATTACHMENTTYPE
,      INVOICINGMETHOD
,      ISACCOUNTANT
,      ISAGENCY
,      ISBANK
,      ISCOMPETITOR
,      ISEXTRADUTY
,      ISMAILING
,      ISMEMBER
,      ISPILOT
,      ISPURCHASE
,      ISRESELLER
,      ISSALES
,      ISSUPPLIER
,      LANGUAGE
,      OINNUMBER
,      LANGUAGEDESCRIPTION
,      LATITUDE
,      LEADPURPOSE
,      LEADSOURCE
,      LOGO
,      LOGOFILENAME
,      LOGOTHUMBNAILURL
,      LOGOURL
,      LONGITUDE
,      MainContact
,      MODIFIED
,      MODIFIER
,      MODIFIERFULLNAME
,      NAME
,      PARENT
,      PAYASYOUEARN
,      PAYMENTCONDITIONPURCHASE
,      PAYMENTCONDITIONPURCHASEDESCRIPTION
,      PAYMENTCONDITIONSALES
,      PAYMENTCONDITIONSALESDESCRIPTION
,      PHONE
,      PHONEEXTENSION
,      POSTCODE
,      PRICELIST
,      PurchaseCurrency
,      PURCHASECURRENCYDESCRIPTION
,      PURCHASELEADDAYS
,      PurchaseVATCode
,      PURCHASEVATCODEDESCRIPTION
,      RECEPIENTOFCOMMISSIONS
,      REMARKS
,      RESELLER
,      RESELLERCODE
,      RESELLERNAME
,      RSIN
,      SALESCURRENCY
,      SALESCURRENCYDESCRIPTION
,      SALESTAXSCHEDULE
,      SALESTAXSCHEDULECODE
,      SALESTAXSCHEDULEDESCRIPTION
,      SalesVATCode
,      SALESVATCODEDESCRIPTION
,      SEARCHCODE
,      SECURITYLEVEL
,      SEPARATEINVPERPROJECT
,      SEPARATEINVPERSUBSCRIPTION
,      SHIPPINGLEADDAYS
,      SHIPPINGMETHOD
,      STARTDATE
,      STATE
,      STATENAME
,      STATUS
,      STATUSSINCE
,      TRADENAME
,      TYPE
,      UNIQUETAXPAYERREFERENCE
,      VATLIABILITY
,      VATNUMBER
,      WEBSITE
from   testset10000@inmemorystorage 

local remark EB10

bulk insert into exactonlinerest..accounts
select ACCOUNTANT
,      ACCOUNTMANAGER
,      ACCOUNTMANAGERFULLNAME
,      ACCOUNTMANAGERHID
,      ACTIVITYSECTOR
,      ACTIVITYSUBSECTOR
,      ADDRESSLINE1
,      ADDRESSLINE2
,      ADDRESSLINE3
,      BLOCKED
,      BRIN
,      BUSINESSTYPE
,      CANDROPSHIP
,      CHAMBEROFCOMMERCE
,      CITY
,      Classification
,      Classification1
,      Classification2
,      Classification3
,      Classification4
,      Classification5
,      Classification6
,      Classification7
,      Classification8
,      CLASSIFICATIONDESCRIPTION
,      Code
,      CODEATSUPPLIER
,      COMPANYSIZE
,      CONSOLIDATIONSCENARIO
,      CONTROLLEDDATE
,      COSTCENTER
,      COSTCENTERDESCRIPTION
,      COSTPAID
,      COUNTRY
,      COUNTRYNAME
,      CREATED
,      CREATOR
,      CREATORFULLNAME
,      CREDITLINEPURCHASE
,      CREDITLINESALES
,      CURRENCY
,      CUSTOMERSINCE
,      DATEVCREDITORCODE
,      DATEVDEBTORCODE
,      DISCOUNTPURCHASE
,      DISCOUNTSALES
,      Division
,      DOCUMENT
,      DUNSNUMBER
,      EMAIL
,      ENDDATE
,      ESTABLISHEDDATE
,      FAX
,      GLACCOUNTPURCHASE
,      GLACCOUNTSALES
,      GLAP
,      GLAR
,      HASWITHHOLDINGTAXSALES
,      ID
,      IGNOREDATEVWARNINGMESSAGE
,      INTRASTATAREA
,      INTRASTATDELIVERYTERM
,      INTRASTATSYSTEM
,      INTRASTATTRANSACTIONA
,      INTRASTATTRANSACTIONB
,      INTRASTATTRANSPORTMETHOD
,      INVOICEACCOUNT
,      INVOICEACCOUNTCODE
,      INVOICEACCOUNTNAME
,      INVOICEATTACHMENTTYPE
,      INVOICINGMETHOD
,      ISACCOUNTANT
,      ISAGENCY
,      ISBANK
,      ISCOMPETITOR
,      ISEXTRADUTY
,      ISMAILING
,      ISMEMBER
,      ISPILOT
,      ISPURCHASE
,      ISRESELLER
,      ISSALES
,      ISSUPPLIER
,      LANGUAGE
,      OINNUMBER
,      LANGUAGEDESCRIPTION
,      LATITUDE
,      LEADPURPOSE
,      LEADSOURCE
,      LOGO
,      LOGOFILENAME
,      LOGOTHUMBNAILURL
,      LOGOURL
,      LONGITUDE
,      MainContact
,      MODIFIED
,      MODIFIER
,      MODIFIERFULLNAME
,      NAME
,      PARENT
,      PAYASYOUEARN
,      PAYMENTCONDITIONPURCHASE
,      PAYMENTCONDITIONPURCHASEDESCRIPTION
,      PAYMENTCONDITIONSALES
,      PAYMENTCONDITIONSALESDESCRIPTION
,      PHONE
,      PHONEEXTENSION
,      POSTCODE
,      PRICELIST
,      PurchaseCurrency
,      PURCHASECURRENCYDESCRIPTION
,      PURCHASELEADDAYS
,      PurchaseVATCode
,      PURCHASEVATCODEDESCRIPTION
,      RECEPIENTOFCOMMISSIONS
,      REMARKS
,      RESELLER
,      RESELLERCODE
,      RESELLERNAME
,      RSIN
,      SALESCURRENCY
,      SALESCURRENCYDESCRIPTION
,      SALESTAXSCHEDULE
,      SALESTAXSCHEDULECODE
,      SALESTAXSCHEDULEDESCRIPTION
,      SalesVATCode
,      SALESVATCODEDESCRIPTION
,      SEARCHCODE
,      SECURITYLEVEL
,      SEPARATEINVPERPROJECT
,      SEPARATEINVPERSUBSCRIPTION
,      SHIPPINGLEADDAYS
,      SHIPPINGMETHOD
,      STARTDATE
,      STATE
,      STATENAME
,      STATUS
,      STATUSSINCE
,      TRADENAME
,      TYPE
,      UNIQUETAXPAYERREFERENCE
,      VATLIABILITY
,      VATNUMBER
,      WEBSITE
from   testset10000@inmemorystorage 

local remark EX1

bulk insert into UploadXMLTopics
( topic
, payload
, division_code
, fragment_payload_flag
, fragment_max_size_characters
)
select 'Accounts'
,      xml
,      spn.code
,      true
,      50000
from   testsetxmlaccounts1000@inmemorystorage
join   ( select code from systempartitions@datadictionary spn where spn.is_selected = true limit 10 ) spn

local remark SQL Server test cases

local log on

create or replace table outputtable
as
select ACCOUNTANT
,      ACCOUNTMANAGER
,      ACCOUNTMANAGERFULLNAME
,      ACCOUNTMANAGERHID
,      ACTIVITYSECTOR
,      ACTIVITYSUBSECTOR
,      ADDRESSLINE1
,      ADDRESSLINE2
,      ADDRESSLINE3
,      BLOCKED
,      BRIN
,      BUSINESSTYPE
,      CANDROPSHIP
,      CHAMBEROFCOMMERCE
,      CITY
,      Classification
,      Classification1
,      Classification2
,      Classification3
,      Classification4
,      Classification5
,      Classification6
,      Classification7
,      Classification8
,      CLASSIFICATIONDESCRIPTION
,      Code
,      CODEATSUPPLIER
,      COMPANYSIZE
,      CONSOLIDATIONSCENARIO
,      CONTROLLEDDATE
,      COSTCENTER
,      COSTCENTERDESCRIPTION
,      COSTPAID
,      COUNTRY
,      COUNTRYNAME
,      CREATED
,      CREATOR
,      CREATORFULLNAME
,      CREDITLINEPURCHASE
,      CREDITLINESALES
,      CURRENCY
,      CUSTOMERSINCE
,      DATEVCREDITORCODE
,      DATEVDEBTORCODE
,      DISCOUNTPURCHASE
,      DISCOUNTSALES
,      Division
,      DOCUMENT
,      DUNSNUMBER
,      EMAIL
,      ENDDATE
,      ESTABLISHEDDATE
,      FAX
,      GLACCOUNTPURCHASE
,      GLACCOUNTSALES
,      GLAP
,      GLAR
,      HASWITHHOLDINGTAXSALES
,      ID
,      IGNOREDATEVWARNINGMESSAGE
,      INTRASTATAREA
,      INTRASTATDELIVERYTERM
,      INTRASTATSYSTEM
,      INTRASTATTRANSACTIONA
,      INTRASTATTRANSACTIONB
,      INTRASTATTRANSPORTMETHOD
,      INVOICEACCOUNT
,      INVOICEACCOUNTCODE
,      INVOICEACCOUNTNAME
,      INVOICEATTACHMENTTYPE
,      INVOICINGMETHOD
,      ISACCOUNTANT
,      ISAGENCY
,      ISBANK
,      ISCOMPETITOR
,      ISEXTRADUTY
,      ISMAILING
,      ISMEMBER
,      ISPILOT
,      ISPURCHASE
,      ISRESELLER
,      ISSALES
,      ISSUPPLIER
,      LANGUAGE
,      OINNUMBER
,      LANGUAGEDESCRIPTION
,      LATITUDE
,      LEADPURPOSE
,      LEADSOURCE
,      LOGO
,      LOGOFILENAME
,      LOGOTHUMBNAILURL
,      LOGOURL
,      LONGITUDE
,      MainContact
,      MODIFIED
,      MODIFIER
,      MODIFIERFULLNAME
,      NAME
,      PARENT
,      PAYASYOUEARN
,      PAYMENTCONDITIONPURCHASE
,      PAYMENTCONDITIONPURCHASEDESCRIPTION
,      PAYMENTCONDITIONSALES
,      PAYMENTCONDITIONSALESDESCRIPTION
,      PHONE
,      PHONEEXTENSION
,      POSTCODE
,      PRICELIST
,      PurchaseCurrency
,      PURCHASECURRENCYDESCRIPTION
,      PURCHASELEADDAYS
,      PurchaseVATCode
,      PURCHASEVATCODEDESCRIPTION
,      RECEPIENTOFCOMMISSIONS
,      REMARKS
,      RESELLER
,      RESELLERCODE
,      RESELLERNAME
,      RSIN
,      SALESCURRENCY
,      SALESCURRENCYDESCRIPTION
,      SALESTAXSCHEDULE
,      SALESTAXSCHEDULECODE
,      SALESTAXSCHEDULEDESCRIPTION
,      SalesVATCode
,      SALESVATCODEDESCRIPTION
,      SEARCHCODE
,      SECURITYLEVEL
,      SEPARATEINVPERPROJECT
,      SEPARATEINVPERSUBSCRIPTION
,      SHIPPINGLEADDAYS
,      SHIPPINGMETHOD
,      STARTDATE
,      STATE
,      STATENAME
,      STATUS
,      STATUSSINCE
,      TRADENAME
,      TYPE
,      UNIQUETAXPAYERREFERENCE
,      VATLIABILITY
,      VATNUMBER
,      WEBSITE
,      rowid$ id2
from   testset10000@inmemorystorage 
where  false

local remark SI1

insert into outputtable
select ACCOUNTANT
,      ACCOUNTMANAGER
,      ACCOUNTMANAGERFULLNAME
,      ACCOUNTMANAGERHID
,      ACTIVITYSECTOR
,      ACTIVITYSUBSECTOR
,      ADDRESSLINE1
,      ADDRESSLINE2
,      ADDRESSLINE3
,      BLOCKED
,      BRIN
,      BUSINESSTYPE
,      CANDROPSHIP
,      CHAMBEROFCOMMERCE
,      CITY
,      Classification
,      Classification1
,      Classification2
,      Classification3
,      Classification4
,      Classification5
,      Classification6
,      Classification7
,      Classification8
,      CLASSIFICATIONDESCRIPTION
,      Code
,      CODEATSUPPLIER
,      COMPANYSIZE
,      CONSOLIDATIONSCENARIO
,      CONTROLLEDDATE
,      COSTCENTER
,      COSTCENTERDESCRIPTION
,      COSTPAID
,      COUNTRY
,      COUNTRYNAME
,      CREATED
,      CREATOR
,      CREATORFULLNAME
,      CREDITLINEPURCHASE
,      CREDITLINESALES
,      CURRENCY
,      CUSTOMERSINCE
,      DATEVCREDITORCODE
,      DATEVDEBTORCODE
,      DISCOUNTPURCHASE
,      DISCOUNTSALES
,      Division
,      DOCUMENT
,      DUNSNUMBER
,      EMAIL
,      ENDDATE
,      ESTABLISHEDDATE
,      FAX
,      GLACCOUNTPURCHASE
,      GLACCOUNTSALES
,      GLAP
,      GLAR
,      HASWITHHOLDINGTAXSALES
,      ID
,      IGNOREDATEVWARNINGMESSAGE
,      INTRASTATAREA
,      INTRASTATDELIVERYTERM
,      INTRASTATSYSTEM
,      INTRASTATTRANSACTIONA
,      INTRASTATTRANSACTIONB
,      INTRASTATTRANSPORTMETHOD
,      INVOICEACCOUNT
,      INVOICEACCOUNTCODE
,      INVOICEACCOUNTNAME
,      INVOICEATTACHMENTTYPE
,      INVOICINGMETHOD
,      ISACCOUNTANT
,      ISAGENCY
,      ISBANK
,      ISCOMPETITOR
,      ISEXTRADUTY
,      ISMAILING
,      ISMEMBER
,      ISPILOT
,      ISPURCHASE
,      ISRESELLER
,      ISSALES
,      ISSUPPLIER
,      LANGUAGE
,      OINNUMBER
,      LANGUAGEDESCRIPTION
,      LATITUDE
,      LEADPURPOSE
,      LEADSOURCE
,      LOGO
,      LOGOFILENAME
,      LOGOTHUMBNAILURL
,      LOGOURL
,      LONGITUDE
,      MainContact
,      MODIFIED
,      MODIFIER
,      MODIFIERFULLNAME
,      NAME
,      PARENT
,      PAYASYOUEARN
,      PAYMENTCONDITIONPURCHASE
,      PAYMENTCONDITIONPURCHASEDESCRIPTION
,      PAYMENTCONDITIONSALES
,      PAYMENTCONDITIONSALESDESCRIPTION
,      PHONE
,      PHONEEXTENSION
,      POSTCODE
,      PRICELIST
,      PurchaseCurrency
,      PURCHASECURRENCYDESCRIPTION
,      PURCHASELEADDAYS
,      PurchaseVATCode
,      PURCHASEVATCODEDESCRIPTION
,      RECEPIENTOFCOMMISSIONS
,      REMARKS
,      RESELLER
,      RESELLERCODE
,      RESELLERNAME
,      RSIN
,      SALESCURRENCY
,      SALESCURRENCYDESCRIPTION
,      SALESTAXSCHEDULE
,      SALESTAXSCHEDULECODE
,      SALESTAXSCHEDULEDESCRIPTION
,      SalesVATCode
,      SALESVATCODEDESCRIPTION
,      SEARCHCODE
,      SECURITYLEVEL
,      SEPARATEINVPERPROJECT
,      SEPARATEINVPERSUBSCRIPTION
,      SHIPPINGLEADDAYS
,      SHIPPINGMETHOD
,      STARTDATE
,      STATE
,      STATENAME
,      STATUS
,      STATUSSINCE
,      TRADENAME
,      TYPE
,      UNIQUETAXPAYERREFERENCE
,      VATLIABILITY
,      VATNUMBER
,      WEBSITE
,      null
from   testset10000@inmemorystorage 

local remark SB1

bulk insert into outputtable
select ACCOUNTANT
,      ACCOUNTMANAGER
,      ACCOUNTMANAGERFULLNAME
,      ACCOUNTMANAGERHID
,      ACTIVITYSECTOR
,      ACTIVITYSUBSECTOR
,      ADDRESSLINE1
,      ADDRESSLINE2
,      ADDRESSLINE3
,      BLOCKED
,      BRIN
,      BUSINESSTYPE
,      CANDROPSHIP
,      CHAMBEROFCOMMERCE
,      CITY
,      Classification
,      Classification1
,      Classification2
,      Classification3
,      Classification4
,      Classification5
,      Classification6
,      Classification7
,      Classification8
,      CLASSIFICATIONDESCRIPTION
,      Code
,      CODEATSUPPLIER
,      COMPANYSIZE
,      CONSOLIDATIONSCENARIO
,      CONTROLLEDDATE
,      COSTCENTER
,      COSTCENTERDESCRIPTION
,      COSTPAID
,      COUNTRY
,      COUNTRYNAME
,      CREATED
,      CREATOR
,      CREATORFULLNAME
,      CREDITLINEPURCHASE
,      CREDITLINESALES
,      CURRENCY
,      CUSTOMERSINCE
,      DATEVCREDITORCODE
,      DATEVDEBTORCODE
,      DISCOUNTPURCHASE
,      DISCOUNTSALES
,      Division
,      DOCUMENT
,      DUNSNUMBER
,      EMAIL
,      ENDDATE
,      ESTABLISHEDDATE
,      FAX
,      GLACCOUNTPURCHASE
,      GLACCOUNTSALES
,      GLAP
,      GLAR
,      HASWITHHOLDINGTAXSALES
,      ID
,      IGNOREDATEVWARNINGMESSAGE
,      INTRASTATAREA
,      INTRASTATDELIVERYTERM
,      INTRASTATSYSTEM
,      INTRASTATTRANSACTIONA
,      INTRASTATTRANSACTIONB
,      INTRASTATTRANSPORTMETHOD
,      INVOICEACCOUNT
,      INVOICEACCOUNTCODE
,      INVOICEACCOUNTNAME
,      INVOICEATTACHMENTTYPE
,      INVOICINGMETHOD
,      ISACCOUNTANT
,      ISAGENCY
,      ISBANK
,      ISCOMPETITOR
,      ISEXTRADUTY
,      ISMAILING
,      ISMEMBER
,      ISPILOT
,      ISPURCHASE
,      ISRESELLER
,      ISSALES
,      ISSUPPLIER
,      LANGUAGE
,      OINNUMBER
,      LANGUAGEDESCRIPTION
,      LATITUDE
,      LEADPURPOSE
,      LEADSOURCE
,      LOGO
,      LOGOFILENAME
,      LOGOTHUMBNAILURL
,      LOGOURL
,      LONGITUDE
,      MainContact
,      MODIFIED
,      MODIFIER
,      MODIFIERFULLNAME
,      NAME
,      PARENT
,      PAYASYOUEARN
,      PAYMENTCONDITIONPURCHASE
,      PAYMENTCONDITIONPURCHASEDESCRIPTION
,      PAYMENTCONDITIONSALES
,      PAYMENTCONDITIONSALESDESCRIPTION
,      PHONE
,      PHONEEXTENSION
,      POSTCODE
,      PRICELIST
,      PurchaseCurrency
,      PURCHASECURRENCYDESCRIPTION
,      PURCHASELEADDAYS
,      PurchaseVATCode
,      PURCHASEVATCODEDESCRIPTION
,      RECEPIENTOFCOMMISSIONS
,      REMARKS
,      RESELLER
,      RESELLERCODE
,      RESELLERNAME
,      RSIN
,      SALESCURRENCY
,      SALESCURRENCYDESCRIPTION
,      SALESTAXSCHEDULE
,      SALESTAXSCHEDULECODE
,      SALESTAXSCHEDULEDESCRIPTION
,      SalesVATCode
,      SALESVATCODEDESCRIPTION
,      SEARCHCODE
,      SECURITYLEVEL
,      SEPARATEINVPERPROJECT
,      SEPARATEINVPERSUBSCRIPTION
,      SHIPPINGLEADDAYS
,      SHIPPINGMETHOD
,      STARTDATE
,      STATE
,      STATENAME
,      STATUS
,      STATUSSINCE
,      TRADENAME
,      TYPE
,      UNIQUETAXPAYERREFERENCE
,      VATLIABILITY
,      VATNUMBER
,      WEBSITE
,      null
from   testset10000@inmemorystorage 

select * from sessionios@datadictionary where call_safe_name='outputtable'