local remark
local remark Invantive PSQL procedures to assist maintaining a large number
local remark of Invantive Data Hub instances on a server.
local remark
local remark (C) 2000-2019 Invantive Software BV, the Netherlands.
local remark

--
-- Generic settings across all instances.
--
create or replace procedure xxhosting_initialize
is
begin
  set smtp-minimum-deliver-duration-ms@mail 5000;
  set smtp-host-address@mail "secret";
  set smtp-user-name@mail "secret";
  set smtp-password@mail "secret";
  set [mail-from-name]@mail "Invantive Hosting";
  set [mail-from-email]@mail "hosting@invantive.com";
end;

--
-- Check whether an agreement is still active. If not, raise an error.
--
create or replace procedure xxhosting_check_agreement
( p_agreement_code varchar2
)
is
begin
  if sysdate > to_date('20190101', 'YYYYMMDD')
  then
    if p_agreement_code = 'L978814541'
    then
      raise_application_error(-20163, 'Subscription has ended.');
    end if;
  else
    null; -- Nothing to do.
  end if;
end;

--
-- Check whether the current connected user equal a specific
-- user and data container ID. When not, raise an error.
--
create or replace procedure xxhosting_check_user
( p_data_container_id varchar2
, p_log_on_code       varchar2
)
is
begin
  --
  -- Check correct user.
  --
  select raise_error
         ( 'xxive001'
         , 'Connected as user ' 
           || user
           || ', but should have been connected as '
           || p_log_on_code
           || '.'
         )
  from   dual@DataDictionary
  where  user != p_log_on_code
  ;
  --
  -- Check correct data container ID.
  --
  select raise_error
         ( 'xxive002'
         , 'Connected on data container ID ' 
           || sys_context('USERENV', 'DATA_CONTAINER_ID')
           || ', but should have been connected to '
           || p_data_container_id
           || '.'
         )
  from   dual@DataDictionary
  where  sys_context('USERENV', 'DATA_CONTAINER_ID') != p_data_container_id
  ;
end;

--
-- Register in Amazon Cloudwatch the progress of a job.
-- Requires Invantive Customer Service for routing plus
-- an additional subscription.
--
create or replace procedure xxhosting_register_progress
( p_metric_code_prefix   varchar2
, p_message_code_prefix  varchar2
, p_error_count_continue pls_integer
)
is
begin
  insert into auditevents@DataDictionary
  ( is_monitoring
  , metric_name
  , service_name
  , message_code
  , user_message
  , metric_value
  , unit_of_measure
  )
  values
  ( true
  , p_metric_code_prefix || 'ReplicateDuration'
  , 'Hosted'
  , p_message_code_prefix || '001'
  , 'Register duration of job.'
  , sys_context('USERENV', 'SQL_DURATION_SEC')
  , 'Seconds'
  )
  ;
  insert into auditevents@DataDictionary
  ( is_monitoring
  , metric_name
  , service_name
  , message_code
  , user_message
  , metric_value
  , unit_of_measure
  )
  select true
  ,      p_metric_code_prefix || 'ReplicateErrors'
  ,      'Hosted'
  ,      p_message_code_prefix || '002'
  ,      'Register outcome of job.'
  ,      coalesce(p_error_count_continue, 0) + wsc_cnt
  ,      'None'
  from   dc_warning_statistics@datacache
  ;
end;