local remark 
local remark Inlezen lijst van projecten gedefinieerd in Excel kolommenlijst, bestaande uit:
local remark
local remark 1. Project stamgegevens
local remark 2. Project urenboekers, gescheiden door een komma in Excel cel.
local remark 3. Project urensoorten, gescheiden door een komma in Excel cel.
local remark 4. Factuurtermijnen.
local remark
local remark De Excel kolommenlijst een Excel naam hebben 'projecten' zodat
local remark de view 'projecten@ic' de juiste gegevens bevat.
local remark 
local remark De vereiste kolommen zijn:
local remark - Begindatum: startdatum, Excel formaat Datum
local remark - Begrotekosten: begrote kosten (EUR), Excel formaat Getal
local remark - Begroteomzet: begrote opbrengsten (EUR), Excel formaat Getal
local remark - Classificatie: projectclassificatie
local remark - Einddatum: einddatum (exclusief einddatum), Excel formaat Datum
local remark - Internenotities: notities project
local remark - Omschrijvinghuidigboekjaar: omschrijving project
local remark - Prognoseomzetprijsafspraak: budget (EUR), Excel formaat Getal
local remark - Projectcode: projectcode
local remark - Projectleider: volledige naam van projectleider
local remark - Relatiecode: relatiecode
local remark - RestrictieWelkeUrencodeTeBoeken: komma-gescheiden lijst van urensoorten (artikelen)
local remark - Termijn1Bedrag: termijn 1 bedrag (EUR), Excel formaat Getal
local remark - Termijn1Datum: termijn 1 datum, Excel formaat Datum
local remark - Termijn2Bedrag: termijn 2 bedrag (EUR), Excel formaat Getal
local remark - Termijn2Datum: termijn 2 datum, Excel formaat Datum
local remark - Termijn3Bedrag: termijn 3 bedrag (EUR), Excel formaat Getal
local remark - Termijn3Datum: termijn 3 datum, Excel formaat Datum
local remark - Termijn4Bedrag: termijn 4 bedrag (EUR), Excel formaat Getal
local remark - Termijn4Datum: termijn 4 datum, Excel formaat Datum
local remark - Termijn5Bedrag: termijn 5 bedrag (EUR), Excel formaat Getal
local remark - Termijn5Datum: termijn 5 datum, Excel formaat Datum
local remark - Termijn6Bedrag: termijn 6 bedrag (EUR), Excel formaat Getal
local remark - Termijn6Datum: termijn 6 datum, Excel formaat Datum
local remark - TermijnFacturatie: zet op 'Ja' indien er factuurtermijnen ingelezen moeten worden.
local remark - Type: soort project in het Nederland, kies uit: Vaste prijs, Nacalculatie, Niet factureerbaar
local remark - Urenbegroot: aantal begrote uren (aantal)
local remark - Waarschuwingoverschrijvinguren: percentage van begrote uren
local remark - WieMagErOpProjectBoeken: komma-gescheiden lijst van volledige namen van medewerkers
local remark 

local define DIVISION_CODE "ENTER-YOUR-DESIRED-DIVISION-CODE"

--
-- Accountancyprojecten staan allemaal in 1 administratie.
--
use ${DIVISION_CODE}

--
-- Daadwerkelijke verwerking.
--
-- Maak lijst van alle Excel project en Exact Online projecten.
--
-- Deze lijst is omdat hij zowel Excel als Exact Online bevat bruikbaar
-- voor zowel het inlezen alsook het vergelijken van Excel met Exact Online.
--
create or replace table pjtall@inmemorystorage
as
select coalesce(pjteol.code, pjtexcel.projectcode) pjt_code
,      case
       when pjteol.code is null
       then 'Excel'
       when pjtexcel.projectcode is null
       then 'Exact Online'
       else 'Beiden'
       end 
       voorkomen
,      pjtexcel.* prefix with 'excel_'
,      pjteol.*   prefix with 'eol_'
from   projecten@ic pjtexcel
full 
outer
join   exactonlinerest..projects pjteol
on     pjteol.code = pjtexcel.projectcode

--
-- Maak opzoeklijst om gebruikte projectclassificaties op te halen.
-- De API's hebben nog geen API die de classificatie 
-- stamdata beschikbaar maakt.
--
create or replace table pjtclassifications@inmemorystorage
as
select distinct
       eol_classification
,      eol_classificationdescription 
from   pjtall@inmemorystorage
where  eol_classification is not null

--
-- Gebruik geen cache zodat je bij select volgend op insert
-- ook nieuwe projecten ziet.
--
-- Dit kan vanaf 17.24.15 ook met een http_memory_cache hint
-- op een per statement en per alias niveau.
--
set use-http-memory-cache false

insert into exactonlinerest..projects
( account
, classification
, manager
, description
, notes
, startdate
, enddate
, code
, type
, BudgetedAmount
, SalesTimeQuantity
, TimeQuantityToAlert
, BudgetedCosts
, BudgetedRevenue
)
select act.id
,      pcn.eol_classification
,      usrpm.userid projectmanager
,      pjt.excel_omschrijvinghuidigboekjaar description
,      pjt.excel_internenotities notes
,      pjt.excel_begindatum startdate
,      pjt.excel_einddatum enddate
,      pjt.excel_projectcode code
--
-- Vertaal Nederlandse omschrijving naar de project types.
-- De ProjectTypes view van Invantive bevat alleen de Engelse omschrijvingen.
--
-- De waarde -1 zorgt er voor dat als een ander projecttype wordt
-- gebruikt dat dan een foutmelding optreedt.
--
,      case 
       when pjt.excel_type = 'Vaste prijs' then 2
       when pjt.excel_type = 'Nacalculatie' then 3
       when pjt.excel_type = 'Niet factureerbaar' then 4
       else -1
       end
       type
,      excel_prognoseomzetprijsafspraak BudgetedAmount
,      excel_urenbegroot SalesTimeQuantity
,      excel_waarschuwingoverschrijvinguren * excel_urenbegroot TimeQuantityToAlert
,      excel_begrotekosten BudgetedCosts
,      excel_begroteomzet BudgetedRevenue
--
-- De Excel relatiecode bevat soms spaties. Door het knullige aanvullen van relatiecodes
-- in Exact Online kan een join alleen snel zijn als je zuivere relatiecodes aanlevert.
--
from   ( select pjt.*, trim(pjt.excel_relatiecode) excel_relatiecode_trimmed from pjtall@inmemorystorage pjt ) pjt
--
-- In Excel kun je afhankelijk van je validatie ook niet in Exact Online
-- geregistreerde relaties invullen. De left outer join zorgt er voor
-- dat er dan bij relaties geen waarde staat, maar het project verdwijnt
-- in ieder geval niet van de tafel.
--
left
outer
join   exactonlinerest..accounts act
on     act.code = pjt.excel_relatiecode_trimmed
join   pjtclassifications@inmemorystorage pcn
on     pcn.eol_ClassificationDescription = pjt.excel_classificatie
--
-- In Excel kun je afhankelijk van je validatie ook niet in Exact Online
-- geregistreerde projectleiders invullen. De left outer join zorgt er voor
-- dat er dan bij projectleider geen waarde staat, maar het project verdwijnt
-- in ieder geval niet van de tafel.
--
left
outer
join   exactonlinerest..users usrpm
on     usrpm.fullname = pjt.excel_projectleider
--
-- Mogelijk maken incrementeel laden.
-- Reeds geladen project codes worden overgeslagen.
-- Of ze nu correct of FOUT in Exact Online staan.
--
where  pjt.excel_projectcode 
       not in 
       ( select /*+ low_cost */ pjteol.code 
         from   pjtall@inmemorystorage pjt 
         join   exactonlinerest..projects pjteol 
         on     pjteol.code = pjt.excel_projectcode 
       )

--
-- Inlezen urenboekers.
--
insert into ProjectRestrictionEmployees
( project
, employee
)
select pjteol.id
,      emp.id
--,      pjt.excel_projectcode
--,      csv.urenboeker
from   pjtall@inmemorystorage pjt
join   csvtable
       ( passing pjt.excel_WieMagErOpProjectBoeken 
         row delimiter ',' 
         column delimiter '#'
         columns urenboeker varchar2 position 1
       ) csv
join   exactonlinerest..projects pjteol
on     pjteol.code = pjt.excel_projectCode
left
outer
join   exactonlinerest..employees emp
on     emp.fullname = trim(csv.urenboeker)
where  pjt.excel_WieMagErOpProjectBoeken is not null
--
-- Incrementeel laden mogelijk maken: ga
-- reeds geladen urenboekers niet opnieuw inlezen.
--
minus
select project
,      employee
from   ProjectRestrictionEmployees
where  projectcode 
       in 
       ( select excel_projectcode 
         from   pjtall@inmemorystorage 
       )

--
-- Inlezen urensoorten.
--
insert into ProjectRestrictionItems
( project
, item
)
select pjteol.id
,      itm.id
--,      pjt.excel_projectCode
--,      csv.urencode
from   pjtall@inmemorystorage pjt
join   csvtable
       ( passing pjt.excel_RestrictieWelkeUrencodeTeBoeken 
         row delimiter ',' 
         column delimiter '#'
         columns urencode varchar2 position 1
       ) csv
join   exactonlinerest..projects pjteol
on     pjteol.code = pjt.excel_projectCode
left
outer
join   exactonlinerest..items itm
on     itm.description = trim(csv.urencode)
and    itm.unit = 'hour'
where  pjt.excel_RestrictieWelkeUrencodeTeBoeken is not null
--
-- Incrementeel laden mogelijk maken.
--
minus
select prm.project
,      prm.item
from   ProjectRestrictionItems prm
where  prm.projectcode 
       in 
       ( select excel_projectcode 
         from   pjtall@inmemorystorage 
       )

--
-- Inlezen factuurtermijnen.
--
insert into invoiceterms
( amount
, project
, InvoiceDate
)
select excel_termijnbedrag
,      pjteol.id
,      excel_termijndatum
--,      excel_projectcode
from   ( select excel_projectCode
         ,      excel_Termijn1Datum  excel_termijndatum label 'Termijndatum'
         ,      excel_Termijn1Bedrag excel_termijnbedrag label 'Termijnbedrag'
         ,      'Termijn 1' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn1Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
         union all
         select excel_projectCode
         ,      excel_Termijn2Datum
         ,      excel_Termijn2Bedrag
         ,      'Termijn 2' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn2Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
         union all
         select excel_projectCode
         ,      excel_Termijn3Datum
         ,      excel_Termijn3Bedrag
         ,      'Termijn 3' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn3Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
         union all
         select excel_projectCode
         ,      excel_Termijn4Datum
         ,      excel_Termijn4Bedrag
         ,      'Termijn 4' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn4Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
         union all
         select excel_projectCode
         ,      excel_Termijn5Datum
         ,      excel_Termijn5Bedrag
         ,      'Termijn 5' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn5Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
         union all
         select excel_projectCode
         ,      excel_Termijn6Datum
         ,      excel_Termijn6Bedrag
         ,      'Termijn 6' herkomst
         from   pjtall@inmemorystorage
         where  coalesce(excel_Termijn6Bedrag, 0) != 0
         and    excel_termijnfacturatie = 'Ja'
       ) pjt
join   exactonlinerest..projects pjteol
on     pjteol.code = pjt.excel_projectcode
--
-- Incrementeel laden mogelijk maken.
--
minus
select amount
,      project
,      InvoiceDate
from   invoiceterms
--
-- Performance optimalisatie.
--
-- Alleen ophalen factuurtermijnen van projecten die ingelezen
-- kunnen worden.
--
where  project 
       in 
       ( select /*+ low_cost */ pjteol.id 
         from   pjtall@inmemorystorage pjt 
         join   exactonlinerest..projects pjteol 
         on     pjteol.code = pjt.excel_projectcode 
       )