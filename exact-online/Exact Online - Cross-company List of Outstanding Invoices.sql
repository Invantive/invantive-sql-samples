--
-- Overview of all outstanding sales and purchase invoices.
--
-- Choose all Exact Online companies.
--
use all

select /*+ join_set(sie, invoicenumber, 5000) */
       rlt.DivisionLabel
,      'Receivables' type
,      rlt.AccountCode
,      rlt.AccountName
,      rlt.JournalCode
,      rlt.InvoiceNumber
,      rlt.EntryNumber
,      rlt.InvoiceDate
,      rlt.DueDate
,      coalesce(sie.currency, rlt.currencycode) currency
,      rlt.Amount
,      sie.AmountFC
,      sie.VATAmountFC
,      sie.AmountDC
,      sie.VATAmountDC
,      rlt.Description
from   receivableslist rlt
left
outer
join   exactonlinerest..salesinvoices sie
on     sie.division      + 1 = rlt.division + 1
and    sie.invoicenumber     = rlt.invoicenumber
and    sie.journal           = rlt.journalcode
union all
select /*+ join_set(pie, entrynumber, 5000) */
       plt.DivisionLabel
,      'Payables' type
,      plt.AccountCode
,      plt.AccountName
,      plt.JournalCode
,      plt.InvoiceNumber
,      plt.EntryNumber
,      plt.InvoiceDate
,      plt.DueDate
,      coalesce(pieval.currency, plt.currencycode) currency
,      plt.Amount
,      -pieval.AmountFC
,      -pievat.AmountFC
,      -pieval.AmountDC
,      -pievat.AmountDC
,      plt.Description
from   payableslist plt
left
outer
join   exactonlinerest..transactionlines pieval
on     pieval.division    + 1 = plt.division + 1
and    pieval.entrynumber     = plt.invoicenumber
and    pieval.journalcode     = plt.journalcode
and    pieval.linenumber      = 0 /* Total. */
left
outer
join   exactonlinerest..transactionlines pievat
on     pievat.division    + 1 = plt.division + 1
and    pievat.entrynumber     = plt.invoicenumber
and    pievat.journalcode     = plt.journalcode
and    pievat.linenumber      = 9999 /* VAT. */
and    pievat.vattype         != 'P' /* VAT payable. */
order
by     DivisionLabel
,      type
,      journalcode
,      entrynumber