--
-- Exclude blocked divisions where Exact Online customer is not paying
-- invoice or did not grant privileges to accountant.
--
create or replace table blockeddivisions@inmemorystorage
as
select 123123 code /* Can not retrieve mailboxes. */
from   dual@datadictionary
union
select 123123 code /* Insufficient privileges to retrieve: ExactOnlineXML..Settings. */
from   dual@datadictionary

use select division_first_Active from (select division_first_active from AllAdministrationCustomers where division_first_active is not null minus select /*+ low_cost */ code from blockeddivisions@inmemorystorage ) limit 10

set [ignore-http-403-errors] true

select /*+ join_set(mat, mailmessageid, 5000) */ mat.attachment xmlfilecontents
,      mat.division || '-' || AttachmentFileName filename
from   mailmessagesreceived mre
join   MailMessageAttachments mat
on     mat.mailmessageid = mre.id
and    mat.Type in ( 20, 22 ) /* 20: UBL 2.0, 22: Simplerinvoicing 1.0 */
where  mre.SenderMailbox = 'Facturen@ExactOnline.nl'
and    mre.created >= add_months(trunc(sysdate, -1), -3)
and    mat.FileSize > 0

local export documents in xmlfilecontents to "c:\temp" filename automatic

select xte.*
from   ( select replace(replace(replace(replace(replace(rft.file_contents, 'doc:Invoice', 'Invoice'), 'cac:', ''), 'cbc:', ''), 'xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:ccts="urn:oasis:names:specification:ubl:schema:xsd:CoreComponentParameters-2" xmlns:stat="urn:oasis:names:specification:ubl:schema:xsd:DocumentStatusCode-1.0" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"', '')
, 'xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:udt="urn:un:unece:uncefact:data:draft:UnqualifiedDataTypesSchemaModule:2" xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2"', '') file_contents
         from   files('c:\temp', '*.xml', false)@os fle
         join   read_file_text(fle.file_path)@os rft
       ) rft
join   xmltable
       ( '/Invoice/InvoiceLine' 
         passing rft.file_contents
         columns exact_custom_number     varchar path '../AccountingCustomerParty/SupplierAssignedAccountID'
         ,       exact_custom_party_name varchar path '../AccountingCustomerParty/Party/PartyName/Name'
         ,       exact_custom_party_coc  varchar path '../AccountingCustomerParty/Party/PartyIdentification/ID[@schemeAgencyName="KVK"]'
         ,       exact_custom_party_vat  varchar path '../AccountingCustomerParty/Party/PartyIdentification/ID[@schemeAgencyName="BTW"]'
         ,       line_quantity_invoiced  varchar2 path './InvoicedQuantity'
         ,       line_amount             varchar2 path './LineExtensionAmount'
         ,       line_item_description   varchar2 path './Item/Description'
         ,       line_item_name          varchar2 path './Item/Name'
         ,       line_item_name_seller   varchar2 path './Item/SellersItemIdentification/ID'
         ,       invoice_issue_date      datetime path '../IssueDate'
         ,       invoice_note            varchar2 path '../Note'
       ) xte

local export results as "c:\temp\exact-online-licences.xlsx" format xlsx

