﻿local Make a condensed overview of users and their privileges.

--
-- Users and the companies they have access to.
--
create or replace table OwnerUserAdministrations@inmemorystorage
as
--
-- Convert the text '{GUID}' to a GUID by removing the accolades
-- and casting to a GUID.
--
select to_guid(translate(oun.user_id_attr, 'x{}', 'x')) user_id_attr
,      oun.user_fullname
,      oun.administration_code_attr
,      oun.administration_number_attr
,      oun.administration_name
from   OwnerUserAdministrations oun

create or replace table ActiveUserRoles@inmemorystorage
as
select ure.UserID
,      ure.Role
,      ure.Description
,      ury.*
from   userroles ure
left
outer
join   UserRoleProperties ury
on     ury.roleid = ure.role
where  ( ure.EndDate is null or ure.EndDate >= sysdate )

--
-- Overview of users and their roles.
--
select usr.fullname
,      usr.username
,      usr.email
,      substr(usr.email, instr(usr.email, '@') + 1) 
       emaildomain
       label 'Email Domain'
,      usr.startdate
,      usr.lastlogin
,      rlesu.role_superuser_cnt > 0
       usr_classification
       label 'Superuser'
,      case
       when oun.company_cnt is null
       then 'No access'
       when oun.company_cnt <= 10
       then 'Non-accountant'
       when oun.company_cnt >= 100
       then 'Poweruser'
       else 'Accountant'
       end
       usr_classification
       label 'Classification'
,      oun.company_cnt
       label '#Companies Access'
,      ounpc4.company_in_postcode4_cnt
       label '#Postal Areas of Companies'
,      rle.role_all_cnt
       label '#Roles'
,      rlewrite.role_write_cnt
       label '#Roles with Write Access'
,      rlesu.role_superuser_cnt
       label '#Roles with Superuser Access'
from   users usr
left
outer
join   ( select user_id_attr
         ,      count(*) company_cnt
         from   OwnerUserAdministrations@inmemorystorage
         group
         by     user_id_attr
       ) oun
on     oun.user_id_attr = usr.userid
left
outer
join   ( select oun.user_id_attr
         ,      count(distinct postcode4) company_in_postcode4_cnt
         from   OwnerUserAdministrations@inmemorystorage oun
         join   ( select code, substr(postcode, 1, 4) postcode4 from systemdivisions sdn ) sdn
         on     sdn.code = oun.administration_code_attr
         group
         by     oun.user_id_attr
       ) ounpc4
on     ounpc4.user_id_attr = usr.userid
left
outer
join   ( select aue.userid
         ,      count(*) role_all_cnt
         from   activeuserroles@InMemoryStorage aue
         group
         by     aue.userid
       ) rle
on     rle.userid = usr.userid
left
outer
join   ( select aue.userid
         ,      count(*) role_write_cnt
         from   activeuserroles@InMemoryStorage aue
         where  changesdata = true
         group
         by     aue.userid
       ) rlewrite
on     rlewrite.userid = usr.userid
left
outer
join   ( select aue.userid
         ,      count(*) role_superuser_cnt
         from   activeuserroles@InMemoryStorage aue
         where  ( isusermanagement = true
                  or
                  iscompanymanagement = true
                )
         and    changesdata = true
         group
         by     aue.userid
       ) rlesu
on     rlesu.userid = usr.userid
where  usr.lastlogin is not null
order
by     usr.fullname
