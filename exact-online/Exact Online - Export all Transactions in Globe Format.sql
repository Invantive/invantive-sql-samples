﻿local remark
local remark Download all Exact Online financial transactions as one large set
local remark suitable for import into Exact Globe.
local remark
local remark All transaction files downloaded are merged into one large XML
local remark and then split to reduce time needed to manually select all files
local remark and upload the data.
local remark
local remark Requires Invantive SQL.
local remark

local remark
local remark Specify folder into which to place the composed output files.
local remark

local define OUTPUT_FOLDER "c:\temp\out"

local remark
local remark Specify integer of Exact Online division to export.
local remark

local define DIVISION_CODE "102673"

local define YEAR "2018"

local remark First period.

local define PERIOD_FROM "1"

local remark Period up to and including.

local define PERIOD_TO "1"

declare
  g_division_code       number;
  g_year                number;
  g_period_from         number;
  g_period_to           number;
  g_use_disk_cache      boolean;
  --
  l_transaction_id_last number;
  l_ts_d_last           varchar2;
  l_ts_d_next           varchar2;
  l_xml                 varchar2;
  l_page_number         number;
  l_loop                boolean;
begin
  --
  -- Choose division to use.
  --
  g_division_code  := '${DIVISION_CODE}';
  g_year           := '${YEAR}';
  g_period_from    := '${PERIOD_FROM}';
  g_period_to      := '${PERIOD_TO}';
  g_use_disk_cache := false;
  --
  l_ts_d_last := '0x0000000000000000';
  --
  -- Start off after least native platform request made.
  --
  select coalesce(transaction_id_max, 0)
  into   l_transaction_id_last
  from   ( select max(nst.transaction_id) transaction_id_max
           from   NativePlatformScalarRequests nst
         )
  ;
  --
  -- Register some log messages.
  --
  create or replace table output@inmemorystorage as select rpad('*', 4000, '*') txt from dual@datadictionary where false;
  insert into output@inmemorystorage(txt) values('Results in NativePlatformRequests start at transaction ID ' || to_char(l_transaction_id_last+1) || '.');
  --
  -- Fetch all pages.
  --
  l_loop := true;
  l_page_number := 1;
  while l_loop = true and l_page_number < 10000
  loop
    insert into NativePlatformScalarRequests
    ( url
    , http_disk_cache_use
    , http_disk_cache_save
    )
    values 
    ( 'https://start.exactonline.nl/docs/XMLDownload.aspx'
      || '?_Division_='
      || to_char(g_division_code)
      --
      -- Include details.
      --
      || '&Params_details=1'
      --
      -- Do not remove PagedFromUI. For unclear reasons, the Backwards parameter
      -- then no longer has any effect on the output format.
      --
      || '&PagedFromUI=1'
      --
      -- Export in Globe/Synergy XML format.
      --
      || '&Backwards=1'
      --
      -- General Ledger transactions.
      --
      || '&Topic=GLTransactions'
      || '&PageNumber='
      || to_char(l_page_number)
      || '&TSPaging='
      || l_ts_d_last
      || '&Params_YearRange_From='
      || to_char(g_year)
      || '&Params_YearRange_To='
      || to_char(g_year)
      || '&Params_Period_From='
      || to_char(g_period_from)
      || '&Params_Period_To='
      || to_char(g_period_to)
    , g_use_disk_cache
    , g_use_disk_cache
    )
    ;
    select nst.result
    --
    -- Extract reference to next page from ts_d in Topic tag.
    --
    ,      xmltransform
           ( nst.result
           , '<?xml version="1.0" encoding="UTF-8"?>'
             || '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'
             || '<xsl:output method="xml" indent="no" omit-xml-declaration="yes" />'
             || '<xsl:template match="/"><xsl:value-of select="/eExact/Topics/Topic/@ts_d"/>'
             || '</xsl:template>'
             || '</xsl:stylesheet>'
           )
    into   l_xml
    ,      l_ts_d_next
    from   NativePlatformScalarRequests nst
    where  nst.transaction_id = l_transaction_id_last + l_page_number
    ;
    insert into output@inmemorystorage(txt) values ('Request #' || to_char(l_page_number) || ': ' || length(l_xml) || ' characters, next ts_d: ' || l_ts_d_next);
    --
    if l_ts_d_next is null or l_ts_d_next = l_ts_d_last
    then 
      l_loop := false;
    end if;
    --
    l_ts_d_last := l_ts_d_next;
    l_page_number := l_page_number + 1;
  end loop;
end;

--
-- See progress. Take note of transaction ID at which work started.
--
select txt 
from   output@inmemorystorage
order
by     rowid$

select max(to_number(regexp_replace(txt, '^.*at transaction ID ([0-9]+)\.$', '$1')))
from   output@inmemorystorage
where  txt like '%start at transaction ID%'

local define FIRST_TRANSACTION_ID "${outcome:0,0}"

--
-- Export as one file per XML download from Exact Online.
--
select 'export-run-'
       || to_char(sysdate, 'YYYYMMDDHH24MISS')
       || '-part-'
       || row_number()
       || '.xml' filename
,      result
from   ( select * 
         from   NativePlatformScalarRequests
         where  transaction_id >= :FIRST_TRANSACTION_ID
         order
         by     transaction_id
       )

local export documents in result to "${OUTPUT_FOLDER}" filename column filename

--
-- Glue all XML together below the FinEntries level.
--
select '<eExact xsi:noNamespaceSchemaLocation="eExact-Schema.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'
       || chr(13)
       || '<FinEntries>' 
       || chr(13)
       || xml 
       || chr(13)
       || '</FinEntries>'
       || chr(13)
       || '</eExact>' 
       result
,      'combined.xml' filename
from   ( select listagg
                ( xmltransform
                  ( result
                  , '<?xml version="1.0" encoding="UTF-8"?>'
                    || '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'
                    || '<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />'
                    || '<xsl:template match="/eExact/FinEntries/FinEntry"><xsl:copy-of select="."/>'
                    || '</xsl:template>'
                    || '</xsl:stylesheet>'
                  )
                , ''
                ) 
                xml
         from   NativePlatformScalarRequests
         where  transaction_id >= :FIRST_TRANSACTION_ID
       )

local export documents in result to "${OUTPUT_FOLDER}" filename column filename
