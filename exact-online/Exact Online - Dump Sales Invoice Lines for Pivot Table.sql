local remark This query is specifically aiming at very large volumes of small transactions.

select /*+ join_set(sle, invoiceid, 10000) join_set(actinv, id, 10000) join_set(actdlr, id, 10000) join_set(itm, id, 5000) join_set(sor, ordernumber, 10000) */ 
       sie.invoicenumber
,      sie.invoicedate
,      sie.ordernumber
,      actinv.code
,      actinv.name
,      actinv.addressline1
,      actinv.postcode
,      actinv.city
,      actinv.state
,      actinv.country
,      actinv.email
,      actinv.phone
,      actdlr.code
,      actdlr.name
,      actdlr.addressline1
,      actdlr.postcode
,      actdlr.city
,      actdlr.state
,      actdlr.country
,      actdlr.email
,      actdlr.phone
,      sle.quantity
,      sle.itemcode
,      sle.itemdescription
,      sle.amountdc / sle.quantity unitprice
,      sle.vatpercentage
,      sle.vatamountdc
,      sle.amountdc 
,      sle.vatamountdc + sle.amountdc amountdctotal
,      itm.itemgroupcode
,      sor.description
,      itm.costpricestandard
,      sle.quantity * itm.costpricestandard  costpricestandardtotal
from   exactonlinerest..salesinvoices sie
join   exactonlinerest..salesinvoicelines sle
on     sle.invoiceid = sie.invoiceid
join   exactonlinerest..accounts actinv
on     actinv.id = sie.invoiceto
join   exactonlinerest..accounts actdlr
on     actdlr.id = sie.deliverto
join   exactonlinerest..items itm
on     itm.id = sle.item
join   exactonlinerest..salesorders sor
on     sor.ordernumber = sie.ordernumber
where  sie.invoicedate between trunc(sysdate) - 5 and trunc(sysdate)
order
by     sie.invoicenumber
,      sle.linenumber
