﻿local remark Get overview what customers pay early, in time or normally too late.

local remark Circumvent current Exact Online bug on complex OData calls during long sessions.

set join-set-points-per-request 50

local remark Use cache on disk for OData requests in addition to memory cache.

set use-http-disk-cache true

use all

create or replace table slspayments@inmemorystorage
as
--
-- Payments on sales invoices.
--
select msesls.division_code division
,      msesls.pseudo_id pseudo_id
,      msesls.journal_attr sls_journalcode
,      msesls.entry_attr sls_entrynumber
,      msesls.amountdc_attr msesls_amountdc_attr
,      msecash.journal_attr cash_journalcode
,      msecash.entry_attr cash_entrynumber
,      msecash.amountdc_attr cash_total_amount
,      msesls.finyear_attr
,      msesls.finperiod_attr
from   matchsetlines msesls
join   matchsetlines msecash
on     msecash.division_code = msesls.division_code
and    msecash.pseudo_id = msesls.pseudo_id
where  msesls.journal_attr in ( select /*+ low_cost */ code from exactonlinerest..journals where type = 20 )
and    msecash.journal_attr not in ( select /*+ low_cost */ code from exactonlinerest..journals where type = 20 )

select /*+ join_set(act, id, 5000) */ tlesls.divisionlabel
,      tlesls.entrynumber invoicenumber
,      tlesls.accountcode
,      tlesls.accountname
,      act.AddressLine1
,      act.Postcode
,      act.City
,      act.Phone
,      act.Website
,      act.ChamberOfCommerce
,      act.Email
,      act.DunsNumber
,      tlesls.AmountDC AmountDCInvoice
,      rlt.Amount AmountDcOpen
,      tlesls.date invoicedate
,      tlesls.duedate
,      case
       when rlt.amount is null
       then 'Paid'
       when rlt.amount != tlesls.amountdc
       then 'Partial'
       else 'Open'
       end
       InvoiceStatus
,      case
       when rlt.amount is not null and tlesls.duedate < sysdate
       then true
       else false
       end
       InvoiceOverdueFlag
,      case
       when rlt.amount is null
       then null
       else ( tlecf.date - tlesls.duedate )
       end
       DaysOverdueNow
,      case
       when rlt.amount is null
       then ( tlecf.date - tlesls.duedate)
       else 0
       end
       DaysPaymentTooLate
,      tlecf.date cashflow_date
,      cf.msesls_amountdc_attr
,      cf.cash_journalcode
,      cf.cash_entrynumber
,      cf.pseudo_id
from   transactionlinesbulk tlesls
left
outer
join   slspayments@inmemorystorage cf
on     cf.division        = tlesls.division
and    cf.sls_journalcode = tlesls.journalcode
and    cf.sls_entrynumber = tlesls.entrynumber
left
outer
join   transactionlinesbulk tlecf
on     tlecf.division      = cf.division
and    tlecf.entrynumber   = cf.cash_entrynumber
and    tlecf.journalcode   = cf.cash_journalcode
and    tlecf.invoicenumber = tlesls.entrynumber
join   exactonlinerest..accounts act
on     act.id       = tlesls.account
and    act.division = tlesls.division
left
outer
join   receivableslist rlt
on     rlt.entrynumber = tlesls.entrynumber
and    rlt.division = tlesls.division
where  tlesls.date > to_date(year(add_months(sysdate, -12)) || '0101', 'YYYYMMDD')
and    tlesls.journalcode in ( select /*+ low_cost */ code from exactonlinerest..journals where type = 20 )
and    tlesls.linenumber = 0 /* Total. */
--
-- We just want to have the date of the cashflow. Therefore only take the booking on 1300 interim account.
-- Placing it in the left outer join causes the short cut join optimization to stop.
--
and    ( tlecf.id is null or tlecf.OffsetID is not null )
--
