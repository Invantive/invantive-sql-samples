﻿local remark
local remark List of recently modified entries in ledger.
local remark

use all

select tle.divisionlabel
,      tle.Journalcode
,      tle.entrynumber
,      tle.date
,      tle.description
,      tle.modified
--
-- Needs coalesce since users in division 1 sometimes get altered
-- or deleted by Exact Online.
--
,      coalesce(tle.modifierfullname, to_char(tle.modifier)) 
       modifier
       label 'Modifier'
,      tle.created
,      coalesce(tle.creatorfullname, to_char(tle.creator)) 
       creator
       label 'Creator'
,      tle.modified = tle.created and tle.modifier = tle.creator 
       unchanged_flag
       label 'Unchanged?'
from   exactonlinerest..transactionlines tle
where  tle.modified > trunc(sysdate) - 3
order
by     tle.modified desc