﻿--
-- Choose an Exact Online company. When uploading to multiple companies,
-- specify them all as number or use 'use QUERY' syntax.
--
use 868055

--
-- Some dummy data, we will just transfer 1 unit of everything
-- from location '1' to location 'AA 01 04'.
--
create or replace table warehousetransferlines@inmemorystorage
as
select code itemcode
,      1 qty
,      '1' storagelocationfrom
,      'AA 01 04' storagelocationto
from   exactonlinerest..items
where  isstockitem = true

--
-- Create XML payload.
--
create or replace table warehousetransferlinesxml@inmemorystorage
as
select listagg(xml, '') xmllines
from   ( select '<WarehouseTransferLine Line="' || to_char(rowid$+1) || '">'
                || '<Item code="' || itemcode || '"/>'
                || xmlelement('Quantity', qty)
                || '<StorageLocationFrom code="' || storagelocationfrom || '"/>'
                || '<StorageLocationTo code="' || storagelocationto || '"/>'
                || '</WarehouseTransferLine>'
                xml
         from   warehousetransferlines@inmemorystorage
       )

--
-- Upload new warehouse transfer.
--
insert into UploadXMLTopics
( division_code
, topic
, payload
, fragment_payload_flag
, fragment_max_size_characters
)
select 868055 target_division
,      'WarehouseTransfers' topic
,      xmlformat
       ( '<?xml version="1.0" encoding="utf-8"?>'
         || '<eExact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="eExact-XML.xsd">'
         || '<WarehouseTransfers>'
         || '<WarehouseTransfer>'
         || xmlelement('Description', 'Sample')
         || xmlelement('Remark', null)
         || '<WarehouseFrom Code="1"/>'
         || '<WarehouseTo Code="1"/>'
         || xmlelement('EntryDate', trunc(sysdate))
         || xmlelement('PlannedTransferDate', trunc(sysdate))
         || xmllines
         || '</WarehouseTransfer>'
         || '</WarehouseTransfers>'
         || '</eExact>'
       )
       xml
,      true /* Chop in pieces. */
,      50000 /* Of this size so Exact accepts very large with millions of transferlines too. */
--
-- Rate limiting is automatically done as well as cross-company loading.
--
from   warehousetransferlinesxml@inmemorystorage

--
-- Optionally check success and re-try load.
--
select * from uploadxmltopicfragments

--
-- Or use a 'full outer join' or 'minus' on the data already
-- contained. Note that Exact has poor support for storing original
-- system references, so you might want to include some structured data
-- in the Remarks field.
--
select * 
from   warehousetransferlines