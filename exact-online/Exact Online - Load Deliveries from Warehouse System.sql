local remark
local remark Extract deliveries from a warehouse system and send
local remark them as deliveries to Exact Online.
local remark
local remark Requires Invantive SQL.
local remark

local remark
local remark Specify folder into which to place the log files.
local remark

local define LOG_FOLDER "c:\temp"

declare
  l_division_code number;
  l_por_cnt       number;
begin
  --
  -- *** CONFIGURATION ***
  --
  -- Exact Online division code, see 'select * from systemdivisions'.
  --
  l_division_code := 102673;
  --
  -- *** END OF CONFIGURATION ***
  --
  -- Establish a fixed list of orders to
  -- include in creating the deliveries on Exact Online
  --
  -- This ensure that orders created in the source system 
  -- during runtime are not partially included.
  --
  create or replace table orders@inmemorystorage
  as
  select *
  from   apiconnect..phorders@sql por
  where  por.sent = true
  and    por.senttoexact = false
  ;
  --
  -- When there is no work to be done, skip all steps.
  -- Exact Online can't handle topics without payload.
  --
  select count(*)
  into   l_por_cnt
  from   orders@inmemorystorage
  ;
  if l_por_cnt != 0
  then
    --
    -- Create one XML per delivery line.
    --
    create or replace table deliverylinexml@inmemorystorage
    as
    select por.orderid
    ,      por.ordernumber
    ,      ple.orderlinenumber
    ,      '<DeliveryLine '
           || 'SalesOrderNumber="'
           || ple.ordernumber 
           || '" SalesOrderLine="'
           || ple.orderlinenumber
           || '" Quantity="'
           || ple.quantity
           || '" DeliveryDate="'
           --
           -- Delivery date is taken from the order.
           -- Individual lines all ship together.
           --
           || xmlencode(por.deliverydate)
           || '">'
           || '<Item code="'
           || ple.itemcode
           || '" />'
           || case
              when ple.batchnumber is not null
              then '<BatchNumbers>'
                   || '<BatchNumberLine Quantity="'
                   || ple.batchquantity
                   || '">'
                   || '<BatchNumber BatchNumber="'
                   || ple.batchnumber
                   || '" />'
                   || '</BatchNumberLine>'
                   || '</BatchNumbers>'
              end
           || '</DeliveryLine>'
           deliverylinexml
    from   orders@inmemorystorage por
    join   apiconnect..phorderlines@sql ple
    on     ple.orderid = por.orderid
    order
    by     por.ordernumber
    ,      ple.orderlinenumber
    ;
    --
    -- Create one XML for all delivery lines of a delivery.
    --
    create or replace table deliverylinesxml@inmemorystorage
    as
    select orderid
    ,      ordernumber
    ,      listagg(deliverylinexml, '') deliverylinesxml
    from   deliverylinexml@inmemorystorage
    group
    by     orderid
    ,      ordernumber
    ;
    --
    -- Create one XML for one delivery, including the delivery lines.
    --
    create or replace table deliveryxml@inmemorystorage
    as
    select por.orderid
    ,      por.ordernumber
    ,      '<Delivery'
           || ' EntryDate="'
           || xmlencode(por.deliverydate)
           || '"'
           || ' TrackingNumber="'
           || por.trackingnumber
           || '">'
           || '<Account'
           || ' ID="'
           || xmlencode(por.accountid)
           || '"'
           || ' />'
           || dle.deliverylinesxml
           || '</Delivery>' 
           deliveryxml
    from   orders@inmemorystorage por
    join   deliverylinesxml@inmemorystorage dle
    on     dle.orderid = por.orderid
    ;
    --
    -- Create one XML for all deliveries.
    --
    create or replace table deliveriesxml@inmemorystorage
    as
    select listagg(deliveryxml, '') deliveriesxml
    from   deliveryxml@inmemorystorage
    ;
    --
    -- Create a table with all XML uploads to be done.
    --
    -- In this case there is only one topic and one Exact Online
    -- company, but you can upload dozens of topics and companies
    -- in one go.
    --
    create or replace table xmlupload@inmemorystorage
    as
    select 'Deliveries' topic
    ,      'deliveries-' || to_char(sysdate, 'YYYYMMDDHH24MISS') || '.xml' filename label 'File Name'
    ,      l_division_code division
    ,      xmlformat
           ( '<?xml version="1.0" encoding="utf-8"?>'
             || '<eExact xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="eExact-XML.xsd">'
             || '<Deliveries>'
             || deliveriesxml
             || '</Deliveries>'
             || '</eExact>'
           )
           filecontents
           label 'XML Payload'
    from   deliveriesxml@inmemorystorage
    ;
    --
    -- Upload all XML messages into Exact Online.
    --
    insert into UploadXMLTopics@eol
    ( topic
    , payload
    , division_code
    , orig_system_reference
    , fragment_payload_flag
    , fragment_max_size_characters
    , fail_on_error
    )
    select topic
    ,      filecontents
    ,      division
    ,      filename
    --
    -- Try to split into pieces of 25 KB at most.
    --
    ,      true fragment_payload_flag
    ,      25000 fragment_max_size_characters 
    ,      true fail_on_error
    from   xmlupload@inmemorystorage
    order 
    by     division
    ,      filename
    ;
    --
    -- Signal that the orders included
    -- have been uploaded into Exact Online.
    --
    update apiconnect..phorders@sql
    set    senttoexact = true
    where  orderid in ( select orderid from orders@inmemorystorage )
    and    senttoexact != true
    ;
  end if;
end;

--
-- Dump uploaded XML into the log folder.
--
select *
from   xmlupload@inmemorystorage

local export documents in filecontents to "${LOG_FOLDER}" filename column filename

--
-- Dump log of upload process into log folder,
-- including specific parts of the XML that failed
-- loading.
--
select '${LOG_FOLDER}\deliveries-' || to_char(sysdate, 'YYYYMMDDHH24MISS') || '.log' filename label 'File Name'
,      uxt.rownum
,      uxt.transaction_id
,      uxt.fragment_number
,      uxt.topic
,      uxt.successful
,      uxt.division_code
,      uxt.orig_system_reference
,      uxt.orig_system_group
,      uxt.date_started
,      uxt.date_ended
,      uxt.payload_retry_failed
,      uxt.count_success
,      uxt.count_warning
,      uxt.count_error
,      uxt.count_fatal_error
,      uxt.result_errors
,      uxt.result
,      uxt.payload
from   UploadXMLTopicFragments@eol uxt
order
by     uxt.rownum

local export results using filename column filename format csv include technical headers