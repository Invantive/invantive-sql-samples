--
-- Overview of all bank accounts across your companies and their balance.
--
-- Choose all Exact Online companies.
--
use all

--
-- Report the last known balance of all bank accounts.
--
select gat.divisionlabel
,      gat.code
,      gat.description
,      ble.close label 'Close Division Currency'
,      jnl.currency
,      jnl.bankaccountiban
from   balancelines ble
join   exactonlinerest..glaccounts gat
on     gat.type     = 12 /* Bank. */
and    gat.code     = ble.years_balance_code_attr
and    gat.division = to_number(ble.division_code)
join   exactonlinerest..journals jnl
on     jnl.glaccount = gat.id
and    jnl.division  = gat.division
where  ble.prevyears = 0
order
by     gat.divisionlabel
,      gat.code